#!/bin/bash

groupname=$1

group_path="/home/$groupname"

# adding unix group
groupadd $groupname
if [ $? -ne 0 ]
then
    exit 1
fi

if [ ! -d $group_path ]
then
    mkdir $group_path
    chown -R root:$groupname $group_path
    chmod 755 $group_path
else
    exit 2
fi