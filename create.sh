#! /usr/bin/env bash

user="apiUser"

apt install -y pwgen
password=`pwgen -1 20`
echo Password:$password

groupdel $user 2> /dev/null
groupadd -f $user
groupadd api

echo Creating user account for $user
userdir="/home/$user"
# adding unix user
userdel -f -r $user 2>/dev/null
useradd -m -d $userdir -g $user $user
echo $user:$password | chpasswd
chown -R $user:api $userdir
chmod 770 $userdir
# setting bash console
chsh -s /bin/bash $user
# giving sudo permissions
echo "$user    ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

cp * /home/$user/
chmod +x /home/$user/*