#!/bin/bash

username=$1
groupname=$2

usermod -a -G $groupname $username
if [ $? -ne 0 ]
then
    exit 1
fi