#!/bin/bash

username=$1
groupname=$2

deluser $username $groupname
if [ $? -ne 0 ]
then
    exit 1
fi