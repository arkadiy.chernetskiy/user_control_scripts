#!/bin/bash

groupname=$1
group_path="/home/$groupname"

# deleting unix group
groupdel $groupname
if [ $? -ne 0 ]
then
    exit 1
fi

if [ -d $group_path ]
then
    rm -rf $group_path
fi