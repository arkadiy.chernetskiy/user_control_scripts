#!/bin/bash

username=$1
password=$2
groupname=$3
home_path=$4
sudo=$5

#group_path="/home/$groupname"
if [[ "$groupname" == "$username" ]]
then
    groupadd -f $username
fi

# adding unix user
useradd -m -d $home_path/$username -g $groupname $username
if [ $? -ne 0 ]
then
    exit 1
fi

echo $username:$password | chpasswd
chsh -s /bin/bash $username

# sudo
if [ $sudo == "true" ]
then
    # adding sudo rigths
    echo "$username    ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
fi
