#!/bin/bash

username=$1

# delete unix user
userdel -r -f $username

#delete user from sudo
sed -i "/$username    ALL=(ALL) NOPASSWD:ALL/d" /etc/sudoers